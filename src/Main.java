import java.util.ArrayList;
import java.util.HashSet;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public void sortMatrixByRow(int matrix[][], int selectedRow) {
        for (int repeat = 0; repeat < matrix.length; repeat++){
            for (int i = 1; i < matrix.length; i++) {
                if (matrix[selectedRow][i] < matrix[selectedRow][i - 1]){
                    for (int j = 0; j < matrix.length; j++) {
                        int temp = matrix[j][i];
                        matrix[j][i] = matrix[j][i - 1];
                        matrix[j][i - 1] = temp;
                    }
                }
            }
        }
    }

    public void sortMatrixByColumn(int matrix[][], int selectedColumn){
        int currentNumber, currentIndex;
        for (int i = 1; i < matrix.length; i++) {
            currentNumber = matrix[i][selectedColumn];
            int[] currentRow = matrix[i];
            currentIndex = i - 1;
            while ((currentIndex >= 0) && (matrix[currentIndex][selectedColumn] > currentNumber)){
                matrix[currentIndex + 1] = matrix[currentIndex];
                currentIndex--;
            }
            matrix[currentIndex+1] = currentRow;
        }
    }

    public int findFirstPositiveElementIndex(int matrix[][], int currentRow){
        for (int i = 0; i < matrix.length; i++){
            if (matrix[currentRow][i] > 0){
                return i;
            }
        }
        return -1;
    }

    public int findSecondPositiveElementIndex(int matrix[][], int firstPositiveElementIndex, int currentRow){
        for (int i = firstPositiveElementIndex + 1; i < matrix.length; i++){
            if (matrix[currentRow][i] > 0){
                return i;
            }
        }
        return -1;
    }

    public int getSumOfElements(int matrix[][], int firstElement, int secondElement, int currentRow){
        int sumOfElements = 0;
        for (int i = firstElement; i < secondElement + 1; i++){
            sumOfElements += matrix[currentRow][i];
        }
        return sumOfElements;
    }

    public int[][] deleteMaximumElementRowsAndColumns(int matrix[][], int matrixRange){
        int maximumElement = matrix[0][0];
        ArrayList<Integer> arrayOfRows = new ArrayList<Integer>();
        ArrayList<Integer> arrayOfColumns = new ArrayList<Integer>();
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix.length; j++){
                if (maximumElement < matrix[i][j]){
                    maximumElement = matrix[i][j];
                    arrayOfRows.clear();
                    arrayOfRows.add(i);
                    arrayOfColumns.clear();
                    arrayOfColumns.add(j);
                }
                else if (maximumElement == matrix[i][j]){
                    arrayOfRows.add(i);
                    arrayOfColumns.add(j);
                }
            }
        }
        System.out.println("Maximum element is " + maximumElement);
        HashSet<Integer> hashSetRows = new HashSet(arrayOfRows);
        HashSet<Integer> hashSetColumns = new HashSet(arrayOfColumns);
        if (matrix.length - hashSetRows.size() == 0 || matrix.length - hashSetColumns.size() == 0){
            return new int[0][0];
        }
        for(int rows : hashSetRows)
        {
            for (int j = 0; j < matrix.length; j++){
                matrix[rows][j] = matrixRange + 1;
            }
        }
        for (int i = 0; i < matrix.length; i++){
            for(int columns : hashSetColumns)
            {
                matrix[i][columns] = matrixRange + 1;
            }
        }
        int[][] changedMatrix = new int[matrix.length - hashSetRows.size()][matrix.length - hashSetColumns.size()];
        int rowIndex = 0, columnIndex = 0;
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix.length; j++){
                if (matrix[i][j] != matrixRange + 1){
                    changedMatrix[rowIndex][columnIndex++] = matrix[i][j];
                }
                if (columnIndex == changedMatrix[0].length){
                    rowIndex++;
                    columnIndex = 0;
                }
            }
        }
        return changedMatrix;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        Random randomGenerator = new Random();
        System.out.print("Please, enter range of matrix: ");
        int matrixSize = input.nextInt();
        int[][] matrix = new int[matrixSize][matrixSize];
        System.out.print("Enter range of random numbers of the matrix: ");
        int matrixRange = input.nextInt();
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                matrix[i][j] = randomGenerator.nextInt(matrixRange - (-matrixRange)) + (-matrixRange);
            }
        }
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println(" ");
        }
        System.out.print("Choose the column's (row's) number to sort the matrix: ");
        int selectedColumnOrRow = input.nextInt();
        Main main = new Main();
        System.out.println("By which element would you like to sort matrix?\n1 - Row\n2 - Column");
        int selectedElement = input.nextInt();
        switch (selectedElement){
            case 1:
                main.sortMatrixByRow(matrix, selectedColumnOrRow - 1);
                System.out.println("Array is sorted.");
                break;
            case 2:
                main.sortMatrixByColumn(matrix, selectedColumnOrRow - 1);
                System.out.println("Array is sorted.");
                break;
            default:
                System.out.println("Invalid command.");
        }
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println(" ");
        }
        for (int i = 0; i < matrixSize; i++){
            int firstElement = main.findFirstPositiveElementIndex(matrix, i);
            if (firstElement == -1 || firstElement == matrixSize - 1){
                System.out.printf("The sum between 2 positive numbers is impossible for the %d row.\n", i + 1);
            }
            else{
                int secondElement = main.findSecondPositiveElementIndex(matrix, firstElement, i);
                if (secondElement == -1){
                    System.out.printf("Sorry, There is no second positive number in the %d row.\n", i + 1);
                }
                else{
                    System.out.printf("%d. Elements: %d, %d; Sum: %d;\n", i + 1, firstElement + 1, secondElement + 1, main.getSumOfElements(matrix, firstElement, secondElement, i));
                }
            }
        }
        matrix = main.deleteMaximumElementRowsAndColumns(matrix, matrixRange);
        System.out.println("New array looks like this: ");
        for (int i = 0; i < matrix.length; i++){
            for (int j = 0; j < matrix[0].length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }
}
